# magicsquare

## Context

This game was made during my 4th semester at University. It was originally
designed for the class 'Operating Systems' teaching how the Unix system works.
We learned about the filesystem, signals, processes, pipes, the POSIX API..

The subject of this project was to design a magic square in C.

## How to run

Compile and test the code with `make` or simply `cc magicsquare.c`.

Then try the executable with a number like 6, 12,.. the limit is your
imagination and the time you want the process to run.
