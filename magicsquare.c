#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>			// pour les msg d'erreur uniquement
#include <stdnoreturn.h>		// C norme 2011
#include <signal.h>
#include <sys/wait.h>

#define CODE_ERREUR 1
#define INT_L 16

struct matrix{
	int size;
	int* elts;
};

/******************************************************************************
 * Gestion des erreurs
 */

char *prog ;				// nom du programme pour les erreurs
int tube[2];				// tube d'échange de données

noreturn void raler (int syserr, const char *fmt, ...)
{
    va_list ap ;

    va_start (ap, fmt) ;
    fprintf (stderr, "%s: ", prog) ;
    vfprintf (stderr, fmt, ap) ;
    fprintf (stderr, "\n") ;
    va_end (ap) ;

    if (syserr) perror ("") ;

    exit (1) ;
}

noreturn void usage (void)
{
    fprintf (stderr, "usage: %s <n>\n", prog) ;
    exit (1) ;
}

/*	Allouer la matrice, ou renvoyer NULL si un problème
	pour pouvoir le traiter (pouvoir raler ou autre)	*/
struct matrix* initMatrix(int n) {

	struct matrix* res = malloc(sizeof(struct matrix));
	if(!res) return NULL;
	if(!(res->elts = malloc(n*n*sizeof(int))))
		return NULL;
	res->size = n;
	return res;
}

//	Libérer l'espace mémoire de la matrice
void freeMatrix(struct matrix* m) {
	free(m->elts);
	free(m);
}

//	https://en.wikipedia.org/wiki/Magic_square
//	#Method_for_constructing_a_magic_square_of_odd_order
//	Utilisation de la méthode dite siamoise
//	pour construire la matrice carrée
void constrMatrix(struct matrix* m, int d) {
	int i, j, n = m->size;
	for(i=0;i<n;i++){
		for(j=0;j<n;j++) {
			m->elts[i*n+j] = d +
			n * ((i+2+j-1+n/2) % n) + ((1+i+2*(j+1)-2)%n) + 1;
		}
	}
}

//	Affichage de la matrice
void printMatrix(struct matrix* m) {
	int i;
	for(i=0;i<m->size*m->size;i++){
		if(i % m->size == 0 && i != 0) printf("\n");
		printf("%d ",m->elts[i]);
	}
	printf("\n");
}

//	fonction handler
void handler(int signum) {
	(void) signum;
}

/*	création de fils, calcul de leur quadrants
	communication des données dans le tube 
	dès qu'ils ont recu le signal
*/
pid_t quadrant(struct matrix* g, int taille, int n) {
	struct matrix* m;
	int i;
	pid_t pid;
	char buf[INT_L];
	struct sigaction s;
	sigset_t t;
	
	if(sigemptyset(&t) == -1) raler(1, "sigemptyset");
	if(sigaddset(&t, SIGUSR1) == -1) raler(1, "sigaddset");
	if(sigprocmask(SIG_BLOCK, &t, NULL) == -1)
		raler(1, "sigprocmask");
		
	s.sa_handler = handler;
	s.sa_flags = 0;
	if(sigemptyset(&s.sa_mask) == -1) raler(1, "sigemptyset");
	if(sigaction(SIGUSR1, &s, NULL) == -1) raler(1, "sigaction");
	
	switch((pid =fork())) {
		case -1:
			raler(1, "cannot fork");
			break;
		case 0:
			if(close(tube[0]) == -1) raler(1, "cant close tube");
			if(!(m = initMatrix(taille))) raler(1, "cant malloc");
			constrMatrix(m, n);
			freeMatrix(g);
			sigsuspend(&s.sa_mask);
			//exit(1);
			for(i=0;i<m->size*m->size;i++) {
				/* 
				 * memset necessaire pour ne pas envoyer des
				 * valeurs pas initialisées
				 * je place f car ce n'est pas un nombre
				 */
				if(!memset(buf, 'f', INT_L)) raler(1, "cant memset");
				snprintf(buf, INT_L, "%d", m->elts[i]);
				if(write(tube[1], buf, INT_L) == -1) raler(1, "write");
			}
			freeMatrix(m);
			if(close(tube[1]) == -1) raler(1, "cannot close file");
			exit(0);
			break;
		default:
			break;
	}
	return pid;
}

int main(int argc, char* argv[]) {

	struct matrix *m = NULL;
	int n, i, j, k, save, atoibuf, status;
	pid_t pids[4];
	char buf[INT_L];
	struct sigaction p;
	
	if(pipe(tube) == -1) raler(1, "cannot pipe");

	prog = argv[0];
	if(argc != 2) usage();

	n = atoi(argv[1]);

	if(n%4 == 0 || n%2 != 0 || n < 0) usage();
	
	/*	Allouer avant de forker les fils
		Si il y a un probleme, il ne crééra aucun fils */
	if(!(m = initMatrix(n))) raler(1, "cant malloc");
	
	/*	Lancer les 4 fils pour diviser le temps de calcul
		et recuperer leurs pids pour pouvoir les kill */
	for(i=0;i<4;i++){
		pids[i] = quadrant(m, n/2, i*n/2*n/2);
	}
	
	p.sa_handler = handler;
	p.sa_flags = 0;
	if(sigemptyset(&p.sa_mask) == -1 )raler(1, "sigemptyset");
	if(sigaction(SIGCHLD, &p, NULL) == -1) raler(1, "sigaction");
	
	for(i=0;i<4;i++){
		if(kill(pids[i], SIGUSR1) == -1) raler(1, "cannot kill");
		for(j=0;j<n*n/4 / (n/2);j++) {
			for(k=0;k<n*n/4 / (n/2);k++) {
				if(read(tube[0], buf, INT_L) <= 0) {
					raler(1, "cannot read file");
				}
				atoibuf = atoi(buf);
				if(i == 0)
					m->elts[j*m->size+k] = atoibuf;
				else if(i == 1)
					m->elts[(j+n/2)*m->size+k+n/2] = atoibuf;
				else if(i == 2) 
					m->elts[(j)*m->size+k+n/2] = atoibuf;
				else if(i == 3)
					m->elts[(j+n/2)*m->size+k] = atoibuf;
			}
		}
		if(wait(&status) == -1) raler(1, "cant wait");
			if(WEXITSTATUS(status) && WEXITSTATUS(status) != 0)
				exit(1);
	}

	if(close(tube[1]) == -1) raler(1, "cannot close file");
	if(close(tube[0]) == -1) raler(1, "cannot close file");
	
	/*	Reconstruire le carré en échangant les colonnes coté gauche
		selon la méthode donnée dans le sujet */
	for(i=0;i<n/2;i++) {
		for(j=0;j<n/4;j++) {
			if(i == n/4) j++;
			save = m->elts[i*m->size+j];
			m->elts[i*m->size+j] = m->elts[(i+n/2)*m->size+j];
			m->elts[(i+n/2)*m->size+j] = save;
			if(i == n/4) j--;
		}
	}
	
	/*	Reconstruire le carré en échangant les colonnes coté droit
		selon la méthode donnée dans le sujet */
	for(i=0;i<n/2;i++) {
		for(j=n/2+n/4+2;j<n;j++){
			save = m->elts[i*m->size+j];
			m->elts[i*m->size+j] = m->elts[(i+n/2)*m->size+j];
			m->elts[(i+n/2)*m->size+j] = save;
		}
	}

	printMatrix(m);
	freeMatrix(m);

	//	si on arrive jusque là, on exit 0
	exit(0);
}
