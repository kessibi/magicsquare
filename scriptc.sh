#!/bin/bash
magic=0
result=`echo "$1*($1*$1+1)/2" | bc`
for i in `seq 1 $1`; do
  magic=0 
  c=`cat result | cut -d " " -f$i`
  for j in $c; do
	   magic=$(($magic + $j))
  done
  if [ $magic -ne $result ] ;then
    exit 1
  fi
done
